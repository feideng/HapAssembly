
extern int char2int(char ch);
extern char int2char(int i);
extern int min2(int a, int b);
extern void initializePowerTwo();
extern int string2int(char *ch);


extern void preScanFile(char* filename, int* num_snp, int* num_reads, int* largest_sp);
extern void sort_reads(char *filename);
extern void remove_single_snp_reads(char *filename);
extern void findBlocks2(char *filename, char* dirname);
extern void preprocessBlock(char *filename, char *filename2);

extern void generateNewDataset(char *filename);

extern void testOnBlock(char *filename, char* filename2, int numOfRepeat, int numSelected);

void setBoundOfCoverage(int coverageValue);