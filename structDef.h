#ifndef _STRUCTDEF_H_
#define _STRUCTDEF_H_



//used for refineHap to avoid repeated I/O
typedef struct _frag{
	int start;
	int end;
	char *content;
}Frag;



//data structure for link list node
typedef struct _node{
	int index;
	struct _node *next;
}Node;



//data structure for link list
typedef struct _list{
	Node *head;//pointer to the head of list
	Node *tail;//pointer to the tail of list
}List;


//used in filterData
typedef struct _fragment{
	int startPosition;
	int length;
	int index;
	int cutPosition;
	int isPairEnd;
	char content[500];
}fragment;

//used in filterData
typedef struct _fragCoverASNP{
	int index;
	int value;
	struct _fragCoverASNP *next;
}fragCoverASNP;


//used in filterData
typedef struct _fragCoverASNPList{
	fragCoverASNP *head;
	fragCoverASNP *tail;
}fragCoverASNPList;

#endif