#ifndef _HAPASSEMBLY_
#define _HAPASSEMBLY_

#include "structDef.h"

/*data structure for reads covering a snp*/
typedef struct _readsCoverASNP{
	int readIndex;
	int value;
	int alsoCover1;//if the read also covers the previous snp, alsoCover1=1
	int alsoCover2;//if the read also covers the following snp, alsoCover2=1
	struct _readsCoverASNP *next;
}readsCoverASNP;


typedef struct _readsCoverASNPList{
	readsCoverASNP *head;
	readsCoverASNP *tail;
}readsCoverASNPList;


typedef struct _costOfASNP{
	int cost;
	int backtrack;
}costOfASNP;


#endif