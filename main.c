#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<time.h>
#include "global.h"
#include "externfunc.h"
#include "externVar.h"



/*
cut a chromosome into blocks
Params: fname - file name for the chromosome
		dirname - the file for storing the blocks, e.g., dirname = "chr1"
*/
void chrToBlocks(char* fname, char* dirname)
{
	//sort the reads based on the start position
	sort_reads(fname);

	//remove reads containing a single snp
	//remove_single_snp_reads("_sortedData.matrix");

	//cut the chromosome into blocks
	findBlocks2("_sortedData.matrix", dirname);
}


/*
reset the snp index of the reads, such that the snp index of the block starts from 1
Params: fname - input filename
		fname2 - output filename
*/
void alignBlock(char* fname, char* fname2)
{
	preprocessBlock(fname, fname2);
}


/*
compute the heuristic solution, the MEC cost is output to screen
Params: fname - input filename
		fname2 - file for storing the solution
*/
void getSolution(char* fname, char* fname2, int numOfRepeat, int numSelected, int coverageValue)
{
	setBoundOfCoverage(coverageValue);
	testOnBlock(fname, fname2, numOfRepeat, numSelected);
}



int main(int argc, char* argv[])
{
	//chrToBlocks(argv[1], argv[2]);
	//alignBlock(argv[1], argv[2]);
	if (argc != 6){
		printf("Usage: %s input_file_name  output_file_name integer1 integer2 boudOfCoverage\n", argv[0]);
		printf("Note: interger1>0, integer2>0 and integer2<= integer1\n");
		exit(0);
	}
	if (atoi(argv[3]) < atoi(argv[4])){
		printf("Error: integer1 should be no smaller than integer2\n");
		exit(0);
	}
	getSolution(argv[1], argv[2], atoi(argv[3]), atoi(argv[4]), atoi(argv[5]));

	//getSolution("test.matrix", "a.txt", 100, 10);
	return 0;

}