#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include"structDef.h"


void preScanFile(char* filename, int* num_snp, int* num_reads, int* largest_sp);

//data structure for reads, used for sorting reads
typedef struct _read{
	int starting_position;
	int len;
	char content[500];
}Read;

/*****************************************************

Scan file to get the total number of SNPs and reads

*****************************************************/
void preScanFile(char* filename, int* num_snp, int* num_reads, int* largest_sp)
{
	FILE* inFile;
	int isPairEnd;
	int startPos;
	int temp;
	int status=0;
	char s1[500], s2[500], s3[500];
	char chr;

	int numOfSNP=0;
	int numOfReads=0;
	int largest_start_position=1;

	inFile=fopen(filename,"r");
	if (inFile==NULL){
		printf("Error: cannot open file!\n");
		exit(0);
	}
	
	status=fscanf(inFile,"%s",s1);
	while (status!=EOF){
		isPairEnd=0;
		numOfReads++;
		
		status=fscanf(inFile,"%s%s",s2,s3);
		startPos=atoi(s2);
		if(largest_start_position < startPos)
			largest_start_position = startPos;
		
		chr = fgetc(inFile);
		while (chr != '\n' && chr != '\r'){
			isPairEnd = 1;
			status=fscanf(inFile,"%s%s",s1,s2);
			chr = fgetc(inFile);
		}


		if(isPairEnd)
			temp=atoi(s1)+strlen(s2)-1;
		else
			temp=startPos+strlen(s3)-1;
		if(numOfSNP<temp)
			numOfSNP=temp;

		status=fscanf(inFile,"%s",s1);
	}

	*num_snp = numOfSNP;
	*num_reads = numOfReads;
	*largest_sp = largest_start_position;
	
	//printf("num read=%d\n", numOfReads);
	//printf("num snps=%d\n", numOfSNP);

	fclose(inFile);
}


/*remove reads covering only one SNP from the data file*/
void remove_single_snp_reads(char *filename)
{
	int status=0;
	char nextChar;
	char s1[500], s2[500], s3[500];
	FILE *inFile, *outFile;
	char chr;

	inFile=fopen(filename,"r");
	outFile=fopen("noSingleton.matrix", "w+");
	if(inFile==NULL || outFile==NULL){
		printf("Error: cannot open file!\n");
		exit(0);
	}

	status=fscanf(inFile,"%s",s1);
	while(status!=EOF){
		status=fscanf(inFile,"%s%s",s2,s3);
		
		nextChar=fgetc(inFile);
		if(nextChar=='\n' || nextChar == '\r'){//not paired end reads
			if(strlen(s3)>1)
				fprintf(outFile,"%s  %s %s\n",s1,s2,s3);
		}
		else{//paired end reads
			fprintf(outFile,"%s  %s %s",s1,s2,s3);
			status=fscanf(inFile,"%s%s",s1,s2);
			fprintf(outFile," %s %s", s1,s2);

			chr = fgetc(inFile);
			while (chr != '\n' && chr != '\r'){
				status=fscanf(inFile,"%s%s",s1,s2);
				fprintf(outFile," %s %s", s1,s2);
				chr = fgetc(inFile);
			}
			fprintf(outFile,"\n");
		}
		status=fscanf(inFile,"%s",s1);
	}

	fclose(inFile);
	fclose(outFile);
}



/*
	sort reads by their starting positions
	Notes:	It needs the parameter largest_start_position
			we can also compute the largest_start_positionst value in this method
*/
void sort_reads(char *filename)
{
	int len;
	int i,j;
	int count=0;
	int temp;
	int isPairEnd=0;
	int status=0;
	char s1[500], s2[500], s3[500];
	Node *newNode,*nextNode;
	Node *node1,*node2;	//used when free memory
	Read *read_in_chr;//array for storing the reads, one item for one read
	List *readList;//one item for one variant, use list to store the index of reads starting from the variant 
	FILE *inFile, *outFile;
	char chr;

	int numOfReads = 0;
	int numOfSNP = 0;
	int largest_start_position = 1;

	preScanFile(filename, &numOfSNP, &numOfReads, &largest_start_position);//

	inFile=fopen(filename,"r");
	outFile=fopen("_sortedData.matrix","w+");
	if(inFile==NULL || outFile==NULL){
		printf("Error: cannot open file!\n");
		exit(0);
	}

	//initialize read_in_chr[i]
	read_in_chr=(Read *)malloc(sizeof(Read)*numOfReads);
	for(i=0;i<numOfReads;i++)
		strcpy(read_in_chr[i].content,"");

	//initialize readList, the 0th item is not used
	readList=(List *)malloc(sizeof(List)*(largest_start_position+1));
	for(i=0;i<=largest_start_position;i++){
		newNode=(Node *)malloc(sizeof(Node));
		newNode->next=NULL;
		readList[i].head=newNode;
		readList[i].tail=newNode;
	}

	//read data into read_in_chr and fill in readList
	status=fscanf(inFile,"%s",s1);
	while(status!=EOF){
		isPairEnd=0;
		status=fscanf(inFile,"%s%s",s2,s3);
		temp=atoi(s2);
		read_in_chr[count].starting_position=temp;

		//fill in readList[]
		newNode=(Node *)malloc(sizeof(Node));
		newNode->index=count;
		newNode->next=NULL;
		//add the first node
		if(readList[temp].head==readList[temp].tail){
			readList[temp].head->next=newNode;
			readList[temp].tail=newNode;
		}
		else{//add new node from tail
			readList[temp].tail->next=newNode;
			readList[temp].tail=newNode;
		}
		
		//fill in read_in_chr[].content
		strcat(read_in_chr[count].content,s1);
		strcat(read_in_chr[count].content,"  ");
		strcat(read_in_chr[count].content,s2);
		strcat(read_in_chr[count].content," ");
		strcat(read_in_chr[count].content,s3);
		chr = fgetc(inFile);
		while(chr!='\n' && chr!='\r'){
			isPairEnd=1;
			status=fscanf(inFile,"%s%s",s1,s2);
			strcat(read_in_chr[count].content," ");
			strcat(read_in_chr[count].content,s1);
			strcat(read_in_chr[count].content," ");
			strcat(read_in_chr[count].content,s2);
			chr = fgetc(inFile);
		}
		strcat(read_in_chr[count].content,"\n");
		if(isPairEnd)
			len=atoi(s1)+strlen(s2)-temp;
		else
			len=strlen(s3);
		read_in_chr[count].len=len;

		status=fscanf(inFile,"%s",s1);
		count++;	
	}

	//sort by the startPosition of the read
	for(i=1;i<=largest_start_position;i++){
		nextNode=readList[i].head->next;
		while(nextNode!=NULL){
			j=nextNode->index;
			fprintf(outFile,"%s",read_in_chr[j].content);
			nextNode=nextNode->next;
		}
	}

	//free memory
	for(i=0;i<=largest_start_position;i++){
		node1=readList[i].head;
		node2=node1->next;
		free(node1);
		while(node2!=NULL){
			node1=node2;
			node2=node2->next;
			free(node1);
		}
	}

	free(readList);
	free(read_in_chr);

	fclose(inFile);
	fclose(outFile);
}


/*separate input data into blocks, blocks are separated by a blank line*/
void findBlocks(char *filename)
{
	int i,j;
	int temp;
	int numOfBlocks=1;
	int isPairedEnd=0;//if the read is paired end, isPairedEnd=1
	int largest_snp_covered=1;//the largest snp a read covers
	int location_covered=1;//the largest snp a block covers
	int status=0;//if status=eof, reach the end of file
	char s1[500], s2[500], s3[500];
	FILE *inFile, *outFile;
	int lastBlock=1;
	char chr;

	
	inFile=fopen(filename,"r");
	outFile=fopen("_block.matrix","w+");
	if(inFile==NULL || outFile==NULL){
		printf("Error: cannot open file!\n");
		exit(0);
	}

	status=fscanf(inFile,"%s",s1);
	while(status!=EOF){
		isPairedEnd=0;
		status=fscanf(inFile,"%s%s",s2,s3);
		temp=atoi(s2);

		//if find a block, print an extra "\n"
		if(location_covered<temp){
			
			printf("%d  ", location_covered-lastBlock+1);//
			lastBlock=temp;//
			fprintf(outFile,"\n");
			numOfBlocks++;
		}
		
		fprintf(outFile,"%s  %s %s",s1,s2,s3);
		chr = fgetc(inFile);
		while(chr!='\n' && chr!='\r'){
			isPairedEnd=1;
			status=fscanf(inFile,"%s%s",s1,s2);
			fprintf(outFile," %s %s",s1,s2);
			chr = fgetc(inFile);
		}
		fprintf(outFile,"\n");

		if(isPairedEnd==0)
			largest_snp_covered=temp+strlen(s3)-1;
		else
			largest_snp_covered=atoi(s1)+strlen(s2)-1;

		if(location_covered < largest_snp_covered)
			location_covered=largest_snp_covered;

		status=fscanf(inFile,"%s",s1);
	}

	printf("numofblocks=%d\n",numOfBlocks);

	fclose(inFile);
	fclose(outFile);
}


/*write each block to a separate file*/
void findBlocks2(char *filename, char* dirname)
{
	int i,j;
	int temp;
	int numOfBlocks=1;
	int isPairedEnd=0;//if the read is paired end, isPairedEnd=1
	int largest_snp_covered=1;//the largest snp a read covers
	int location_covered=1;//the largest snp a block covers
	int status=0;//if status=eof, reach the end of file
	char s1[500], s2[500], s3[500];
	FILE *inFile, *outFile;
	int lastBlock=1;
	char str1[500];//char str1[]="data/block";
	char str2[]=".matrix";
	char str3[500];
	int first = 1;
	char chr;

	inFile=fopen(filename,"r");
	if(inFile==NULL){
		printf("Error: cannot open file!\n");
		exit(0);
	}
	strcpy(str1, "");
	sprintf(str1, "%s%s", dirname, "/block");

	sprintf(str3, "%s%d%s", str1, numOfBlocks, str2);
	outFile=fopen(str3, "w+");

	status=fscanf(inFile,"%s",s1);
	while(status!=EOF){
		isPairedEnd=0;
		status=fscanf(inFile,"%s%s",s2,s3);
		temp=atoi(s2);
		if (first == 1 && temp > 1){
				location_covered=temp;
		}
		first = 0;

		//if find a block, print an extra "\n"
		if(location_covered<temp){
			
			//printf("%d  ", location_covered-lastBlock+1);//
			lastBlock=temp;//
			//fprintf(outFile,"\n");
			numOfBlocks++;

			//a file for the new block
			fclose(outFile);
			sprintf(str3, "%s%d%s", str1, numOfBlocks, str2);
			outFile=fopen(str3, "w+");
		}
		
		fprintf(outFile,"%s  %s %s",s1,s2,s3);
		chr = fgetc(inFile);
		while(chr!='\n' && chr!='\r'){
			isPairedEnd=1;
			status=fscanf(inFile,"%s%s",s1,s2);
			fprintf(outFile," %s %s",s1,s2);
			chr = fgetc(inFile);
		}
		fprintf(outFile,"\n");

		if(isPairedEnd==0)
			largest_snp_covered=temp+strlen(s3)-1;
		else
			largest_snp_covered=atoi(s1)+strlen(s2)-1;

		if(location_covered < largest_snp_covered)
			location_covered=largest_snp_covered;

		status=fscanf(inFile,"%s",s1);
	}

	//printf("numofblocks=%d\n",numOfBlocks);

	fclose(inFile);
	fclose(outFile);
}


/*preprocess a blcok to change its startSNP to 1*/
void preprocessBlock(char *filename, char *filename2)
{
	int tmp;
	int status;
	int isPairEnd;
	int startSNP;
	char s1[500],s2[500],s3[500];
	FILE *fp1, *fp2;
	int overlap_len=0;
	char chr;

	fp1=fopen(filename,"r");
	fp2=fopen(filename2,"w+");
	if(fp1==NULL || fp2==NULL){
		printf("Error: cannot open file!\n");
		exit(0);
	}

	status=fscanf(fp1,"%s%s",s1,s2);
	startSNP=atoi(s2);
	startSNP=startSNP-1;
	
	fseek(fp1,0,SEEK_SET);
	status=fscanf(fp1,"%s",s1);
	while(status!=EOF){
		isPairEnd=0;
		status=fscanf(fp1,"%s%s",s2,s3);
		fprintf(fp2,"%s  %d %s",s1,atoi(s2)-startSNP,s3);
		chr = fgetc(fp1);
		while(chr!='\n' && chr!='\r'){
			isPairEnd=1;
			status=fscanf(fp1,"%s%s",s1,s2);
			fprintf(fp2," %d %s",atoi(s1)-startSNP,s2);
			chr = fgetc(fp1);
		}
		fprintf(fp2,"\n");
		status=fscanf(fp1,"%s",s1);
	}

	fclose(fp1);
	fclose(fp2);
}
