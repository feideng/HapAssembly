#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "externVar.h"


int char2int(char ch);
char int2char(int i);
int min2(int a, int b);
static int powerOfTwo(int n);
void initializePowerTwo();
int string2int(char *ch);
void setBoundOfCoverage(int coverageValue);


int char2int(char ch)
{
	return (int)ch-48;
}



/*
	convert int to char
*/
char int2char(int i)
{
	return i+48;
}



/*
	find minimum between two integers
*/
int min2(int a, int b)
{
	if(a<=b)
		return a;
	else
		return b;
}



/*
	compute pow(2,n)
*/
static int powerOfTwo(int n)
{
	int i;
	int result=1;
	for(i=0;i<n;i++)
		result=result*2;
	return result;
}

/*compute all possible power of two*/
void initializePowerTwo()
{
	int i;

	powerTwo = (int *)malloc(sizeof(int)*(boundOfCoverage+1));
	
	for (i=0; i<=boundOfCoverage; i++)
		powerTwo[i] = powerOfTwo(i);

}

/*
	covert a string to int, each character in the string is either 0 or 1
	view the string as a binary representation, e.g. if string is "101", then output 5
*/
int string2int(char *ch)
{
	int i;
	int len;
	int result=0;
	int tmp;

	len=strlen(ch);
	if(!len){
		printf("Error:");
		exit(0);
	}
	else{
		for(i=0;i<len;i++){
			tmp=(int) (ch[i]-48); //tmp=char2int(ch[i]);
			result=result<<1;
			result=result+tmp;
		}
		return result;
	}
}

void setBoundOfCoverage(int coverageValue)
{
	boundOfCoverage = coverageValue;
}