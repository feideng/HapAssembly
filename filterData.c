#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "structDef.h"
#include "externfunc.h"
#include "externVar.h"


static void addToFragList2(fragCoverASNPList *fragList2, int snpIndex, fragCoverASNP *newFrag);
static void computeCoverage(char *filename, fragCoverASNPList *fragList2, fragment *frag, int *coverage);
static void randomNumGenerator(int n, int *randList);
static void cutFrag(int fragIndex, int cutPosition, fragCoverASNPList *fragList2, fragment *frag, int *coverage);
static void filterDataset(char *filename, int numOfSNP, fragCoverASNPList *fragList2, fragment *frag, int *coverage, int *randList);
void generateNewDataset(char *filename);


/*add a node to snp list*/
static void addToFragList2(fragCoverASNPList *fragList2, int snpIndex, fragCoverASNP *newFrag)
{
	if(fragList2[snpIndex].head == fragList2[snpIndex].tail){
		fragList2[snpIndex].head->next = newFrag;
		fragList2[snpIndex].tail = newFrag;
	}
	else{
		fragList2[snpIndex].tail->next = newFrag;
		fragList2[snpIndex].tail = newFrag;
	}
}

/*compute coverage for each snp*/
static void computeCoverage(char *filename, fragCoverASNPList *fragList2, fragment *frag, int *coverage)
{
	int i;
	int status = 0;
	int fragIndex = 0;
	int isPairEnd;
	int startSNP;
	int length;
	char s1[500], s2[500], s3[500];
	FILE *inFile;
	fragCoverASNP *newFrag;
	char chr;


	inFile = fopen(filename,"r");
	if(inFile == NULL){
		printf("Error: cannot open file!\n");
		exit(0);
	}

	status = fscanf(inFile,"%s",s1);
	while (status != EOF){
		isPairEnd = 0;
		status = fscanf(inFile,"%s%s",s2,s3);
		startSNP = atoi(s2);

		frag[fragIndex].startPosition = startSNP;
		frag[fragIndex].index = fragIndex;
		strcat(frag[fragIndex].content, s1);
		strcat(frag[fragIndex].content, "  ");
		strcat(frag[fragIndex].content, s2);
		strcat(frag[fragIndex].content, " ");
		strcat(frag[fragIndex].content, s3);

		chr = fgetc(inFile);
		while (chr !='\n' && chr!='\r'){
			isPairEnd = 1;
			fscanf(inFile, "%s%s", s1, s2);
			strcat(frag[fragIndex].content, " ");
			strcat(frag[fragIndex].content, s1);
			strcat(frag[fragIndex].content, " ");
			strcat(frag[fragIndex].content, s2);
			chr = fgetc(inFile);
		}
		strcat(frag[fragIndex].content, "\n");

		if (isPairEnd)
			length = atoi(s1) + strlen(s2) - startSNP;
		else
			length = strlen(s3);

		frag[fragIndex].length = length;
		frag[fragIndex].isPairEnd = isPairEnd;

		for (i=startSNP; i<startSNP+length; i++){
			coverage[i]++;
			newFrag = (fragCoverASNP *)malloc(sizeof(fragCoverASNP));
			newFrag->index = fragIndex;
			newFrag->next = NULL;
			addToFragList2(fragList2, i, newFrag);
		}

		status = fscanf(inFile,"%s",s1);
		fragIndex++;
	}

	fclose(inFile);
}


/*randomly select boundOfCoverage numbers from numbers*/
static void randomNumGenerator(int n, int *randList)
{
	int i,j;
	int *tempList;
	int count;
	tempList = (int *)malloc(sizeof(int)*(n+1));
	
	for(i=0; i<=n; i++)
		tempList[i] = -1;
	for(i=0; i<=boundOfCoverage+1; i++)
		randList[i] = -1;

	count = 0;
	while (count < boundOfCoverage){
		j = 1 + rand()%n; //j=1+(int)(n*rand()/(RAND_MAX+1.0)); //
		if (tempList[j] == -1){
			tempList[j] = j;
			count++;
		}
	}

	j = 1;
	for (i=1; i<=n; i++){
		if (tempList[i] != -1){
			randList[j] = i;
			j++;
		}
	}

	randList[boundOfCoverage+1] = -1;
	free(tempList);
}


/*cut a frag*/
static void cutFrag(int fragIndex, int cutPosition, fragCoverASNPList *fragList2, fragment *frag, int *coverage)
{
	int i,j;
	int len;
	int span;
	int start;
	int find;
	fragCoverASNP *frag1, *frag2;

	len = frag[fragIndex].length;
	start = frag[fragIndex].startPosition;
	span = start + len - 1;

	//remove the frag from the fragList[i],
	//the snp fragIndex is handled in filterData()
	for(i=cutPosition+1; i<=span; i++){
		coverage[i]--;
		frag1 = fragList2[i].head;
		frag2 = fragList2[i].head->next;
		find = 0;//indicate whether find the frag in the list of snp i

		while (frag2 != NULL && find==0 ){
			j = frag2->index;
			if (j == fragIndex){
				find = 1;
				frag1->next = frag2->next;//remove the element from the corresponding fragList2[i]
				free(frag2);//free the node!!!!!!!!
				break;
			}
			else{
				frag1 = frag2;
				frag2 = frag2->next;
			}
		}
	}
}


/*filter data set*/
static void filterDataset(char *filename, int numOfSNP, fragCoverASNPList *fragList2, fragment *frag, int *coverage, int *randList)
{
	int i,j,k,p,q;
	fragCoverASNP *newFrag;
	fragCoverASNP *newFrag2;

	computeCoverage(filename, fragList2, frag, coverage);

	for(i=1;i<=numOfSNP;i++){
		if(coverage[i]>boundOfCoverage){
			randomNumGenerator(coverage[i], randList);
			newFrag=fragList2[i].head->next;
			newFrag2=fragList2[i].head;

			j=1;//index for randList[]
			k=randList[j];
			p=0;
			while(newFrag!=NULL){
				p++;
				if(p==k){//picked reads
					j++;
					k=randList[j];//update k
					newFrag2=newFrag;
					newFrag=newFrag->next;
				}
				else{//unpicked reads
					q=newFrag->index;
					frag[q].cutPosition=i;
					newFrag2->next=newFrag->next;//delete the node from list
					free(newFrag);//free the node!!!!!!!!
					newFrag=newFrag2->next;//newFrag=newFrag->next;
					cutFrag(q,i, fragList2, frag, coverage);
				}
			}
			coverage[i]=boundOfCoverage;
		}
	}
}


/*generate new dataset*/
void generateNewDataset(char *filename)
{
	int i;
	int status=0;
	int index;
	int startSNP;
	int isPairEnd;
	int t1,t2,t3;
	int cutPos;
	char tmp[500];
	char s1[500], s2[500], s3[500];
	FILE *inFile, *outFile;
	char chr;
	int numOfSNP=0;
	int numOfReads=0;
	int largest_start_position=1;
	int *coverage;
	int *randList;
	fragment *frag;
	fragCoverASNPList *fragList2;
	fragCoverASNP *newFrag, *frag1,*frag2;

	preScanFile(filename, &numOfSNP, &numOfReads, &largest_start_position);

	/*initialize parameter*/
	randList = (int *)malloc(sizeof(int)*(boundOfCoverage+2));//!!!!!!!
	coverage = (int *)malloc(sizeof(int)*(numOfSNP+1));
	fragList2 = (fragCoverASNPList *)malloc(sizeof(fragCoverASNPList)*(numOfSNP+1));
	for (i=0; i<=numOfSNP; i++){
		coverage[i] = 0;
		newFrag = (fragCoverASNP *)malloc(sizeof(fragCoverASNP));
		newFrag->next = NULL;
		fragList2[i].head = newFrag;
		fragList2[i].tail = newFrag;
	}

	frag = (fragment *)malloc(sizeof(fragment)*numOfReads);
	for (i=0; i<numOfReads; i++){
		frag[i].cutPosition = -1;
		frag[i].isPairEnd = 0;
		strcpy(frag[i].content, "");
	}/*end of initialization*/

	filterDataset(filename, numOfSNP, fragList2, frag, coverage, randList);

	inFile = fopen(filename, "r");
	outFile = fopen("_randomData.matrix", "w+");
	if (inFile==NULL || outFile==NULL){
		printf("Error: cannot open file!\n");
		exit(0);
	}

	index = 0;
	status = fscanf(inFile,"%s",s1);
	while (status != EOF){
		isPairEnd = 0;
		cutPos = frag[index++].cutPosition;
		status = fscanf(inFile,"%s%s",s2,s3);
		
		if (cutPos == -1){//no need to cut
			fprintf(outFile,"%s  %s %s",s1,s2,s3);
			chr = fgetc(inFile);
			while (chr!='\n' && chr!='\r'){
				status = fscanf(inFile,"%s%s",s1,s2);
				fprintf(outFile," %s %s",s1,s2);
				chr = fgetc(inFile);
			}
			fprintf(outFile,"\n");
		}
		else{
			startSNP = atoi(s2);
			t1 = strlen(s3);
			if (cutPos < startSNP + t1){//cut from from segment
				strcpy(tmp,"");
				strncat(tmp, s3, cutPos-startSNP);
				if (strlen(tmp) > 0)
					fprintf(outFile,"%s  %s %s\n",s1,s2,tmp);
				chr = fgetc(inFile);
				while (chr!='\n' && chr!='\r'){
					status = fscanf(inFile,"%s%s",s1,s2);
					chr = fgetc(inFile);
				}
			}
			else{
				fprintf(outFile,"%s  %s %s",s1,s2,s3);
				chr = fgetc(inFile);
				while (chr!='\n' && chr!='\r'){
					fscanf(inFile,"%s%s",s1,s2);
					t1 = atoi(s1);
					t2 = strlen(s2);

					if (cutPos <= t1){
						chr = fgetc(inFile);
						while (chr!='\n' && chr!='\r'){
							status = fscanf(inFile,"%s%s",s1,s2);
							chr = fgetc(inFile);
						}
						break;
					}
					else if (cutPos < t1+t2){
						strcpy(tmp,"");
						strncat(tmp, s2, cutPos-t1);
						if(strlen(tmp) > 0)
							fprintf(outFile," %s %s",s1,tmp);
						chr = fgetc(inFile);
						while (chr!='\n' && chr!='\r'){
							status = fscanf(inFile,"%s%s",s1,s2);
							chr = fgetc(inFile);
						}
						break;
					}
					else
						fprintf(outFile," %s %s",s1,s2);
					chr = fgetc(inFile);
				}
				fprintf(outFile,"\n");
			}
		}
		status = fscanf(inFile,"%s",s1);
	}

	//freeMemory();
	free(coverage);
	free(frag);
	free(randList);
	for(i=0; i<=numOfSNP; i++){
		frag1 = fragList2[i].head;
		frag2 = fragList2[i].head->next;
		free(frag1);
		while (frag2!=NULL){
			frag1 = frag2;
			frag2 = frag2->next;
			free(frag1);
		}
	}
	free(fragList2);
	//end of free memory

	fclose(inFile);
	fclose(outFile);
}
