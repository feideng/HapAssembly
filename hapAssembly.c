#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "externVar.h"
#include "externfunc.h"
#include "hapAssembly.h"

static void freeMemory(int numOfSNPInOrigin, readsCoverASNPList *snpList, costOfASNP **costList, int *numOfReadsCoverASNP, int *finalPartition, int* minInPrevious, int* minPartitionInPrevious);
static void addToSNPList(int snpIndex, readsCoverASNP *newRead, readsCoverASNPList *snpList);
static void findReadsCoverASNP(char *filename, readsCoverASNPList *snpList, int *numOfReadsCoverASNP);
static int computeCostForAColumn(int snpIndex, int partition, readsCoverASNPList *snpList, int *numOfReadsCoverASNP);
static int hasOverlapWithPrevious(int snpIndex, readsCoverASNPList *snpList);
static int findCurrentCost(int snpIndex, int partition, int *numOfReadsCoverASNP, costOfASNP **costList);
static void setFinalPartition(int snpIndex, int partition, readsCoverASNPList *snpList, int *numOfReadsCoverASNP, int *finalPartition);
static void computeFinalPartition(int partitionWithMinCost, int startPos, int endPos, costOfASNP **costList, readsCoverASNPList *snpList, int *numOfReadsCoverASNP, int *finalPartition);
static void computeHaplotype(int numOfSNPInOrigin, readsCoverASNPList *snpList, int *numOfReadsCoverASNP, int *finalPartition, int* h0, int* h1);
static void fillCost(int snpIndex, costOfASNP **costList, int *numOfReadsCoverASNP);
static int findRestriction(int snpIndex, int partition, int *overlapIndex, int num_overlap, int *numOfReadsCoverASNP);
static void computeMinInPrevious(int snpIndex,costOfASNP **costList, readsCoverASNPList *snpList, int *numOfReadsCoverASNP, int* minInPrevious, int* minPartitionInPrevious);
static int findOverlapIndex(int snpIndex, int* overlapIndex, readsCoverASNPList *snpList);
static void firstStep(char *filename, int numOfSNPInOrigin, int* h0, int* h1);
static int computeDistance(int index, int hap, Frag *fragList, int* h0, int* h1);
static Frag* loadReads(char *filename, int numOfSNPInOrigin, int numOfReadsInOrigin);
static int getFinalCost(int numOfReadsInOrigin, Frag* fragList, int* h0, int* h1);
static int refineHap_oneLoop(char *filename, int numOfSNPInOrigin, int numOfReadsInOrigin, Frag* fragList, int* h0, int* h1, int* h0_2, int* h1_2);
static int refineHap(char *filename, int numOfSNPInOrigin, int numOfReadsInOrigin, Frag* fragList, int* h0, int* h1);
static int compDistanceFor2Hap(int *haplotype0, int *haplotype1, int numOfSNPInOrigin);
void testOnBlock(char *filename, char* filename2, int numOfRepeat, int numSelected);



static void freeMemory(int numOfSNPInOrigin, readsCoverASNPList *snpList, costOfASNP **costList, int *numOfReadsCoverASNP, int *finalPartition, int* minInPrevious, int* minPartitionInPrevious)
{
	int i;

	readsCoverASNP *read1, *read2;

	//free each snpList[i]
	for(i=0;i<=numOfSNPInOrigin;i++){
		read1=snpList[i].head;
		read2=read1->next;
		free(read1);
		while(read2!=NULL){
			read1=read2;
			read2=read2->next;
			free(read1);
		}
	}

	free(snpList);
	free(costList);
	free(numOfReadsCoverASNP);
	free(finalPartition);
	free(minInPrevious);
	free(minPartitionInPrevious);
}

/*
	add a read to the list associated with the snp indexed by snpIndex
	Params:	snpIndex	-	the index of the snp
			newRead		-	the read to be added
	Notes:	add from tail
*/
static void addToSNPList(int snpIndex, readsCoverASNP *newRead, readsCoverASNPList *snpList)
{
	//add the first read to the list
	if(snpList[snpIndex].head==snpList[snpIndex].tail){
		snpList[snpIndex].head->next=newRead;
		snpList[snpIndex].tail=newRead;
	}
	else{//not the first read
		snpList[snpIndex].tail->next=newRead;
		snpList[snpIndex].tail=newRead;
	}
}


/*
	find reads covering a snp, one list for one snp
	Notes: before the method is called, we need to know numOfSNP and numOfReads.
		   Do not handle paired end data!
*/
static void findReadsCoverASNP(char *filename, readsCoverASNPList *snpList, int *numOfReadsCoverASNP)
{
	int i, t1,t2,t3;
	int startSNP, status=0;
	int readIndex=0, isPairEnd, length;
	char s1[500], s2[500], s3[500];
	readsCoverASNP *newRead;
	FILE *inFile;
	char chr;

	inFile=fopen(filename,"r");
	if(inFile==NULL){
		printf("Error: cannot open file!\n");
		exit(0);
	}

	status=fscanf(inFile,"%s",s1);
	while(status!=EOF){
		isPairEnd=0;
		status=fscanf(inFile,"%s%s",s2,s3);
		startSNP=atoi(s2);
		t1=strlen(s3);

		//add the read to the list of the first snp it covers
		newRead=(readsCoverASNP *)malloc(sizeof(readsCoverASNP));
		newRead->value=(int)(s3[0]-'0');//char2int(s3[0]);
		newRead->next=NULL;
		newRead->readIndex=readIndex;
		newRead->alsoCover1=0;
		addToSNPList(startSNP,newRead, snpList);
		numOfReadsCoverASNP[startSNP]++;
		
		for(i=1;i<t1;i++){
			newRead=(readsCoverASNP *)malloc(sizeof(readsCoverASNP));
			newRead->value=(int)(s3[i]-'0');
			newRead->next=NULL;
			newRead->readIndex=readIndex;
			addToSNPList(startSNP+i,newRead, snpList);
			numOfReadsCoverASNP[startSNP+i]++;
		}
		
		t2=startSNP+t1;//the next position to be filled
		
		chr = fgetc(inFile);
		while(chr!='\n' && chr!='\r'){
			isPairEnd=1;
			status=fscanf(inFile,"%s%s",s1,s2);
			t3=atoi(s1);

			//positions between [t2,t3-1] are gaps, use -1 to represent
			for(i=t2;i<t3;i++){
				newRead=(readsCoverASNP *)malloc(sizeof(readsCoverASNP));
				newRead->value=-1;
				newRead->next=NULL;
				newRead->readIndex=readIndex;
				addToSNPList(i,newRead, snpList);
				numOfReadsCoverASNP[i]++;
			}

			//fill paired end
			for(i=0;i<strlen(s2);i++){
				newRead=(readsCoverASNP *)malloc(sizeof(readsCoverASNP));
				newRead->next=NULL;
				newRead->value=(int)(s2[i]-'0');
				newRead->readIndex=readIndex;
				addToSNPList(t3+i,newRead, snpList);
				numOfReadsCoverASNP[t3+i]++;
			}

			t2=t3+strlen(s2);//reset the next position to be filled
			chr = fgetc(inFile);
		}
		
		//compute readLength
		if(isPairEnd)
			length=t2-startSNP;
		else
			length=t1;

		//fill in attribute alsoCover1 and alsoCover2
		if(!isPairEnd && t1==1)
			snpList[startSNP].tail->alsoCover2=0;
		else{
			snpList[startSNP].tail->alsoCover2=1;
			for(i=1;i<length;i++){
				snpList[startSNP+i].tail->alsoCover1=1;
				snpList[startSNP+i].tail->alsoCover2=1;
			}
			//the attribute alsoCover2 for the last snp is 0
			snpList[startSNP+i-1].tail->alsoCover2=0;
		}

		readIndex++;
		status=fscanf(inFile,"%s",s1);
	}

	fclose(inFile);
}


/*
	given a partition on snpIndex, compute the cost in the single column
	Params:	snpIndex	-	the index of the snp
			partition	-	the partition on snpIndex
	Returns: the cost
*/
static int computeCostForAColumn(int snpIndex, int partition, readsCoverASNPList *snpList, int *numOfReadsCoverASNP)
{
	int j;
	int h00=0;//num of reads that are assigned to h0 and have a value of 0
	int h01=0;
	int h10=0;
	int h11=0;
	int t1,t2;
	int cost=0;
	readsCoverASNP *newRead;
	
	j=numOfReadsCoverASNP[snpIndex]-1;
	newRead=snpList[snpIndex].head->next;
	while(newRead!=NULL){
		t1=(partition>>j) & 0x01;//get the partition for the read
		t2=newRead->value;
		if(t1==0 && t2==0)
			h00++;
		else if(t1==0 && t2==1)
			h01++;
		else if(t1==1 && t2==0)
			h10++;
		else if(t1==1 && t2==1)
			h11++;
		else//t2 is a gap
			;

		newRead=newRead->next;
		j--;
	}

	//cost=min2(h00,h01)+min2(h10,h11);
	cost=min2(h01+h10, h00+h11);


	return cost;
}


/*
	Test if a snp has overlap with its previous snp
	Params: snpIndex - the index of the snp
	Returns:1 - true, 0 - false
*/
static int hasOverlapWithPrevious(int snpIndex, readsCoverASNPList *snpList)
{
	int result;
	readsCoverASNP *newRead;

	result=0;
	newRead=snpList[snpIndex].head->next;
	while(newRead!=NULL){
		if(newRead->alsoCover1==1){
			result=1;
			break;
		}
		newRead=newRead->next;
	}

	return result;
}


/*
	no overlap between snpIndex and snpIndex+1, find minimum score for the given snp, and add a node to
	backList associated with snpIndex+1 
	Params:	snpIndex - the index of the snp
			partition - a partition on snpIndex+1, used for backtracking
	Returns: the MEC score for the first snpIndex snps
*/
static int findCurrentCost(int snpIndex, int partition, int *numOfReadsCoverASNP, costOfASNP **costList)
{
	int i;
	int minCost=10000000;//initialized to be a very large number
	int totalPartition;
	int partitionWithMinCost;

	totalPartition=powerTwo[numOfReadsCoverASNP[snpIndex]];
	for(i=0;i<totalPartition;i++){
		if(costList[snpIndex][i].cost<minCost){
			minCost=costList[snpIndex][i].cost;
			partitionWithMinCost=i;
		}
	}
	
	costList[snpIndex+1][partition].backtrack=partitionWithMinCost;

	return minCost;
}


/*
	given a snp j and its partition, compute finalPartition[] on the reads cover j
	Notes: if a read covers j and j+1 simultaneously, then the partition of the read
		   will be determined by the partition of j+1, instead of j (backtrack)
*/
static void setFinalPartition(int snpIndex, int partition, readsCoverASNPList *snpList, int *numOfReadsCoverASNP, int *finalPartition)
{
	int i,j,k;
	readsCoverASNP *newRead;

	if(numOfReadsCoverASNP[snpIndex]>0){
		j=numOfReadsCoverASNP[snpIndex]-1;
		newRead=snpList[snpIndex].head->next;
		while(newRead!=NULL){
			k=(partition>>j) & 0x01;
			i=newRead->readIndex;
			finalPartition[i]=k;
			j--;
			newRead=newRead->next;
		}
	}
}


/*
	backtrack from startPos to endPos and set final partition in the interval
	Params:	partitionWithMinCost - a partition on startPos
			startPos - 
			endPos - 
	Notes:	startPos>endPos
*/
static void computeFinalPartition(int partitionWithMinCost, int startPos, int endPos, costOfASNP **costList, readsCoverASNPList *snpList, int *numOfReadsCoverASNP, int *finalPartition)
{
	int i;
	int partition;
	int previousPartition;

	setFinalPartition(startPos,partitionWithMinCost, snpList, numOfReadsCoverASNP, finalPartition);
	previousPartition=partitionWithMinCost;
	for(i=startPos-1;i>=endPos;i--){
		partition=costList[i+1][previousPartition].backtrack;
		setFinalPartition(i,partition, snpList, numOfReadsCoverASNP, finalPartition);
		previousPartition=partition;
	}
}


/*
	compute a pair of haplotype based on the final partition on reads
	Notes:	1: the two haplotypes are in h0[] and h1[]
			2: must compute finalPartition[] before the method is called
*/
static void computeHaplotype(int numOfSNPInOrigin, readsCoverASNPList *snpList, int *numOfReadsCoverASNP, int *finalPartition, int* h0, int* h1)
{
	int i,j,k;
	int value;
	int t00,t01,t10,t11;
	int tmp1, tmp2;
	readsCoverASNP *newRead;

	//initialize haplotypes
	for(i=1;i<=numOfSNPInOrigin;i++){
		h0[i]=-1;
		h1[i]=-1;
	}

	for(i=1;i<=numOfSNPInOrigin;i++){
		if(numOfReadsCoverASNP[i]>0){
			t00=0;	t01=0;	t10=0;	t11=0;

			newRead=snpList[i].head->next;
			while(newRead!=NULL){
				j=newRead->readIndex;
				k=finalPartition[j];
				value=newRead->value;
				if(k==1 && value==1)
					t11++;
				else if(k==1 && value==0)
					t10++;
				else if(k==0 && value==1)
					t01++;
				else if(k==0 && value==0)
					t00++;

				newRead=newRead->next;
			}

			//set h1[i] and h0[i]
			tmp1 = t01 + t10;//h0[i]=0, h1[i]=1
			tmp2 = t00 + t11;//h0[i]=1, h1[i]=0
			if (tmp1 < tmp2){
				h0[i] = 0;
				h1[i] = 1;
			}
			else if (tmp2 < tmp1){
				h0[i] = 1;
				h1[i] = 0;
			}
			else{
				h0[i] = -1;
				h1[i] = -1;
			}

		}
	}
}


/*
	fill the cost and backtrack for the second half, i.e. after having computed 
	the cost from 0~totalPartition/2-1 for the given snp, fill the other half [totalPatition/2,totalPartition-1]
*/
static void fillCost(int snpIndex, costOfASNP **costList, int *numOfReadsCoverASNP)
{
	int i,j,k;
	int totalPartition;
	int totalPartition2;

	totalPartition=powerTwo[numOfReadsCoverASNP[snpIndex]];
	totalPartition2=powerTwo[numOfReadsCoverASNP[snpIndex-1]];

	for(i=totalPartition/2;i<totalPartition;i++){
		j=totalPartition-1-i;
		costList[snpIndex][i].cost=costList[snpIndex][j].cost;

		k=costList[snpIndex][j].backtrack;
		k=totalPartition2-1-k;
		costList[snpIndex][i].backtrack=k;
	}
}

/*given a partition on a column, find the restriction on reads covering two columns*/
static int findRestriction(int snpIndex, int partition, int *overlapIndex, int num_overlap, int *numOfReadsCoverASNP)
{
	int i;
	int value;
	int result=0;
	int coverage=numOfReadsCoverASNP[snpIndex];

	for (i=0; i<num_overlap; i++){
		value=(partition>>(coverage-overlapIndex[i])) &0x01;
		result=(result<<1) + value;
	}

	return result;
}

/*find the min for all possible restrictions*/
static void computeMinInPrevious(int snpIndex,costOfASNP **costList, readsCoverASNPList *snpList, int *numOfReadsCoverASNP, int* minInPrevious, int* minPartitionInPrevious)
{
	int i, j;
	int num_overlap=0;
	int overlapIndex[100];
	int totalPartition=powerTwo[numOfReadsCoverASNP[snpIndex]];
	readsCoverASNP *aread;
	
	i=0;
	aread=snpList[snpIndex].head->next;
	while (aread!=NULL){
		i++;
		if (aread->alsoCover2)
			overlapIndex[num_overlap++]=i;
		aread=aread->next;
	}

	for (i=0; i<powerTwo[num_overlap]; i++)
		minInPrevious[i]=9999999;

	for (i=0; i<totalPartition; i++){
		j=findRestriction(snpIndex, i, overlapIndex, num_overlap, numOfReadsCoverASNP);
		if (costList[snpIndex][i].cost < minInPrevious[j]){
			minInPrevious[j]=costList[snpIndex][i].cost;
			minPartitionInPrevious[j]=i;
		}
	}
}

/*find index of reads on column snpIndex covering both snpIndex and snpIndex-1*/
static int findOverlapIndex(int snpIndex, int* overlapIndex, readsCoverASNPList *snpList)
{
	int i=0;
	int num_overlap=0;
	readsCoverASNP *aread;

	aread=snpList[snpIndex].head->next;
	while (aread!=NULL){
		i++;
		if (aread->alsoCover1)
			overlapIndex[num_overlap++]=i;
		aread=aread->next;
	}

	return num_overlap;
}

/*compute initial solution*/
static void firstStep(char *filename, int numOfSNPInOrigin, int* h0, int* h1)
{
	int i, j, k,t1, t2;
	int cost, coverage,totalPartition=0;
	int previous, current;
	int hasOverlap, num_overlap;
	int restriction;
	int minCost=10000000;
	int partitionWithMinCost;
	int overlapIndex[500];
	int numOfSNP, numOfReads, largest_sp;
	readsCoverASNPList *snpList;
	readsCoverASNP *newRead;
	int *numOfReadsCoverASNP;
	int *finalPartition;
	costOfASNP **costList;
	int *minInPrevious, *minPartitionInPrevious;

	preScanFile(filename, &numOfSNP, &numOfReads, &largest_sp);
	/*initialize();*/
	snpList=(readsCoverASNPList *)malloc(sizeof(readsCoverASNPList)*(numOfSNPInOrigin+1));
	numOfReadsCoverASNP=(int *)malloc(sizeof(int)*(numOfSNPInOrigin+1));
	costList=(costOfASNP **)malloc(sizeof(costOfASNP *)*(numOfSNPInOrigin+1));
	for(i=0;i<=numOfSNPInOrigin;i++){
		newRead=(readsCoverASNP *)malloc(sizeof(readsCoverASNP));
		newRead->next=NULL;
		snpList[i].head=newRead;
		snpList[i].tail=newRead;
		numOfReadsCoverASNP[i]=0;
	}
	finalPartition=(int *)malloc(sizeof(int)*numOfReads);
	for(i=0;i<numOfReads;i++)
		finalPartition[i]=-1;
	minInPrevious=(int *)malloc(sizeof(int)*powerTwo[boundOfCoverage]);
	minPartitionInPrevious=(int *)malloc(sizeof(int)*powerTwo[boundOfCoverage]);
	/*end of initialize*/


	findReadsCoverASNP(filename, snpList, numOfReadsCoverASNP);
	//compute cost for the first snp on each partition
	coverage=numOfReadsCoverASNP[1];
	totalPartition=powerTwo[coverage];
	costList[1]=(costOfASNP *)malloc(sizeof(costOfASNP)*totalPartition);
	for(i=0;i<totalPartition;i++){
		cost=computeCostForAColumn(1,i, snpList, numOfReadsCoverASNP);
		costList[1][i].cost=cost;
		costList[1][i].backtrack=-1;
	}

	j=2;
	previous=1;
	while (j<=numOfSNPInOrigin){
		coverage=numOfReadsCoverASNP[j];
		totalPartition=powerTwo[coverage];
		costList[j]=(costOfASNP *)malloc(sizeof(costOfASNP)*totalPartition);

		if (coverage==0){
			cost=findCurrentCost(j-1,0, numOfReadsCoverASNP, costList);
			costList[j][0].cost=cost;

			//free costList[i] on time
			current=j-1;
			computeFinalPartition(costList[j][0].backtrack,current,previous, costList, snpList, numOfReadsCoverASNP, finalPartition);
			for(i=previous;i<=current;i++)
				free(costList[i]);
			previous=j;
		}
		else{
			hasOverlap=hasOverlapWithPrevious(j, snpList);
			if (!hasOverlap){
				for(i=0;i<totalPartition/2;i++){
					t1=findCurrentCost(j-1,i, numOfReadsCoverASNP, costList);
					t2=computeCostForAColumn(j,i, snpList, numOfReadsCoverASNP);
					cost=t1+t2;
					costList[j][i].cost=cost;
				}
				fillCost(j, costList, numOfReadsCoverASNP);

				//free costList[i] on time
				current=j-1;
				computeFinalPartition(costList[j][0].backtrack,current,previous, costList, snpList, numOfReadsCoverASNP, finalPartition);
				for(i=previous;i<=current;i++)
					free(costList[i]);
				previous=j;
			}
			else{
				computeMinInPrevious(j-1, costList, snpList, numOfReadsCoverASNP, minInPrevious, minPartitionInPrevious);
				num_overlap=findOverlapIndex(j, overlapIndex, snpList);
				for (i=0;i<totalPartition/2;i++){
					restriction=findRestriction(j, i, overlapIndex, num_overlap, numOfReadsCoverASNP);
					t1=minInPrevious[restriction];
					t2=computeCostForAColumn(j,i, snpList, numOfReadsCoverASNP);
					costList[j][i].cost=t1+t2;	
					costList[j][i].backtrack=minPartitionInPrevious[restriction];
				}
				fillCost(j, costList, numOfReadsCoverASNP);
			}
		}
		j++;
	}

	totalPartition=powerTwo[numOfReadsCoverASNP[numOfSNPInOrigin]];
	for(i=0;i<totalPartition;i++){
		if(costList[numOfSNPInOrigin][i].cost<minCost){
			minCost=costList[numOfSNPInOrigin][i].cost;
			partitionWithMinCost=i;
		}
	}

	computeFinalPartition(partitionWithMinCost,numOfSNPInOrigin,previous, costList, snpList, numOfReadsCoverASNP, finalPartition);
	for(i=previous;i<=numOfSNPInOrigin;i++)
		free(costList[i]);

	computeHaplotype(numOfSNPInOrigin, snpList, numOfReadsCoverASNP, finalPartition, h0, h1);

	/*freeMemory();*/
	freeMemory(numOfSNPInOrigin, snpList, costList,numOfReadsCoverASNP, finalPartition, minInPrevious, minPartitionInPrevious);
}

/*
	compute the distance between a string s and the given haplotype
	Params:	start - start snp of the string
			end - last snp of the string
			hap	- the 0th or 1st haplotype
			*s	- the given string
	Returns:the hamming distance

*/
static int computeDistance(int index, int hap, Frag *fragList, int* h0, int* h1)
{
	int i;
	int start, end;
	int dis=0;
	char chr;

	start=fragList[index].start;
	end=fragList[index].end;

	if(hap==0){
		for(i=start;i<=end;i++){
			chr=fragList[index].content[i-start];
			if(h0[i]==0 && chr=='1')
				dis++;
			else if(h0[i]==1 && chr=='0')
				dis++;
		}
	}
	else{
		for(i=start;i<=end;i++){
			chr=fragList[index].content[i-start];
			if(h1[i]==0 && chr=='1')
				dis++;
			else if(h1[i]==1 && chr=='0')
				dis++;
		}
	}

	return dis;
}


/*load reads into memory*/
static Frag* loadReads(char *filename, int numOfSNPInOrigin, int numOfReadsInOrigin)
{
	int i,t1,t2;
	int startSNP, endSNP;
	int status=0, index=0;
	char s1[500], s2[500], s3[500];
	FILE *inFile;
	char chr;
	int *marker;
	Frag* fragList;

	marker=(int *)malloc(sizeof(int)*(numOfSNPInOrigin+1));
	for(i=0;i<=numOfSNPInOrigin;i++)
		marker[i]=-1;
	fragList=(Frag *)malloc(sizeof(Frag)*numOfReadsInOrigin);

	inFile=fopen(filename,"r");
	if(inFile==NULL){
		printf("Error: cannot open file!\n");
		exit(0);
	}

	status=fscanf(inFile,"%s",s1);
	while(status!=EOF){
		status=fscanf(inFile,"%s%s",s2,s3);
		startSNP=atoi(s2);
		t1=strlen(s3);
		t2=startSNP+t1-1;

		for(i=startSNP;i<=t2;i++){
			if(s3[i-startSNP]=='0')
				marker[i]=0;
			else
				marker[i]=1;
		}

		chr = fgetc(inFile);
		while(chr!='\n' && chr!='\r'){
			status=fscanf(inFile,"%s%s",s1,s2);
			t1=atoi(s1);
			t2=t1+strlen(s2)-1;

			for(i=t1;i<=t2;i++){
				if(s2[i-t1]=='0')
					marker[i]=0;
				else
					marker[i]=1;
			}
			chr = fgetc(inFile);
		}
		endSNP=t2;

		fragList[index].start=startSNP;
		fragList[index].end=endSNP;
		fragList[index].content=(char *)malloc(sizeof(char)*(endSNP-startSNP+1+1));
		strcpy(fragList[index].content,"");
		for(i=startSNP;i<=endSNP;i++){
			if(marker[i]==0)
				strcat(fragList[index].content,"0");
			else if(marker[i]==1)
				strcat(fragList[index].content,"1");
			else
				strcat(fragList[index].content,"-");
			marker[i]=-1;
		}

		index++;
		status=fscanf(inFile,"%s",s1);
	}

	free(marker);
	fclose(inFile);

	return fragList;
}

static int getFinalCost(int numOfReadsInOrigin, Frag* fragList, int* h0, int* h1)
{
	int i;
	int cost0, cost1;
	int totalCost = 0;

	for(i=0;i<numOfReadsInOrigin;i++){
		cost0=computeDistance(i,0, fragList, h0, h1);
		cost1=computeDistance(i,1, fragList, h0, h1);
		if (cost0 <= cost1)
			totalCost+=cost0;
		else
			totalCost+=cost1;
	}

	return totalCost;
}

/*refine solution once*/
static int refineHap_oneLoop(char *filename, int numOfSNPInOrigin, int numOfReadsInOrigin, Frag* fragList, int* h0, int* h1, int* h0_2, int* h1_2)
{
	int i,j;
	int start, end;
	int cost0,cost1;
	int totalCost=0;
	int tmp1, tmp2;
	char chr;
	int** c0;
	int** c1;

	c0 = (int **)malloc(sizeof(int *)*(numOfSNPInOrigin+1));
	c1 = (int **)malloc(sizeof(int *)*(numOfSNPInOrigin+1));
	for(i=0;i<=numOfSNPInOrigin;i++){
		c0[i]=(int *)malloc(sizeof(int)*2);
		c1[i]=(int *)malloc(sizeof(int)*2);
	}

	for(i=0;i<=numOfSNPInOrigin;i++){
		c0[i][0]=0;
		c0[i][1]=0;
		c1[i][0]=0;
		c1[i][1]=0;
	}


	for(i=0;i<numOfReadsInOrigin;i++){
		cost0=computeDistance(i,0, fragList, h0, h1);
		cost1=computeDistance(i,1, fragList, h0, h1);

		start=fragList[i].start;
		end=fragList[i].end;

		if(cost0<=cost1){
			totalCost+=cost0;
			for(j=start;j<=end;j++){
				chr=fragList[i].content[j-start];
				if(chr=='0')
					c0[j][0]++;
				else if(chr=='1')
					c0[j][1]++;
			}
		}
		else{
			totalCost+=cost1;
			for(j=start;j<=end;j++){
				chr=fragList[i].content[j-start];
				if(chr=='0')
					c1[j][0]++;
				else if(chr=='1')
					c1[j][1]++;
			}
		}
	}

	for (i=1;i<=numOfSNPInOrigin;i++){
		tmp1 = c0[i][1] + c1[i][0];
		tmp2 = c0[i][0] + c1[i][1];
		if (tmp1 < tmp2){
			h0_2[i] = 0;
			h1_2[i] = 1;
		}
		else if (tmp2 < tmp1){
			h0_2[i] = 1;
			h1_2[i] = 0;
		}
		else{
			h0_2[i] = -1;
			h1_2[i] = -1;
		}
	}
	
	for(i=0;i<=numOfSNPInOrigin;i++){
		free (c0[i]);
		free (c1[i]);
	}
	free(c0);
	free(c1);

	return totalCost;
}


/*refine solutions*/
static int refineHap(char *filename, int numOfSNPInOrigin, int numOfReadsInOrigin, Frag* fragList, int* h0, int* h1)
{
	int i;
	int currentCost;
	int previousCost;
	int hasChange;
	int round=0;
	int maxRound=100;
	int min;
	int *h0_2,*h1_2;
	
	h0_2=(int *)malloc(sizeof(int)*(numOfSNPInOrigin+1));
	h1_2=(int *)malloc(sizeof(int)*(numOfSNPInOrigin+1));

	previousCost=refineHap_oneLoop(filename, numOfSNPInOrigin, numOfReadsInOrigin, fragList, h0, h1, h0_2, h1_2);
	for(i=1;i<=numOfSNPInOrigin;i++){
		h0[i]=h0_2[i];
		h1[i]=h1_2[i];
	}

	hasChange=1;
	while(hasChange && (round<=maxRound)){
		round++;
		currentCost=refineHap_oneLoop(filename, numOfSNPInOrigin, numOfReadsInOrigin, fragList, h0, h1, h0_2, h1_2);

		hasChange=0;
		for(i=1;i<=numOfSNPInOrigin;i++){
			if(h0[i]!=h0_2[i] || h1[i]!=h1_2[i])
				hasChange=1;
			h0[i]=h0_2[i];
			h1[i]=h1_2[i];
		}

	}

	//srand((int)time(0));
	for(i=1;i<=numOfSNPInOrigin;i++){
		if(h0[i]==-1){
			h0[i] = rand()%2; //h0[i]=(int)(2*rand()/(RAND_MAX+1.0));
			h1[i]=1-h0[i];
		}
	}
	
	currentCost = getFinalCost(numOfReadsInOrigin, fragList, h0, h1);	

	free(h0_2);
	free(h1_2);
	//printf("MEC=%d   ",currentCost);
	return currentCost;
}

/*compute distance for two haplotypes*/
static int compDistanceFor2Hap(int *haplotype0, int *haplotype1, int numOfSNPInOrigin)
{
	int i;
	int dis=0;

	for(i=1;i<=numOfSNPInOrigin;i++)
		if(haplotype0[i]!=haplotype1[i])
			dis++;
	
	return dis;
}




/*find the cost for a single block*/
void testOnBlock(char *filename, char* filename2, int numOfRepeat, int numSelected)
{

	int i, j, k, tmp, tmp2;
	int *score, *status, *correspondence;
	int dis0, dis1;
	int selected[100];
	int **hap0, **hap1;
	int *finalHap0, *finalHap1;
	int minScore=99999999;
	int indexOfMinScore=0;
	int count00,count01,count10,count11;
	double rate, finalRate;
	char f1[500], f2[500], f3[500];
	FILE *out;
	int numOfSNPInOrigin, numOfReadsInOrigin, largest_sp;
	Frag *fragList;
	
	initializePowerTwo();
	preScanFile(filename, &numOfSNPInOrigin, &numOfReadsInOrigin, &largest_sp);

	//initialize2();
	fragList=loadReads(filename, numOfSNPInOrigin, numOfReadsInOrigin);	

	
	finalHap0=(int *)malloc(sizeof(int)*(numOfSNPInOrigin+1));
	finalHap1=(int *)malloc(sizeof(int)*(numOfSNPInOrigin+1));
	correspondence=(int *)malloc(sizeof(int)*numSelected);
	score=(int *)malloc(sizeof(int)*numOfRepeat);
	status=(int *)malloc(sizeof(int)*numOfRepeat);
	hap0=(int **)malloc(sizeof(int *)*numOfRepeat);
	hap1=(int **)malloc(sizeof(int *)*numOfRepeat);
	for (i=0;i<numOfRepeat;i++){
		status[i]=0;
		hap0[i]=(int *)malloc(sizeof(int)*(numOfSNPInOrigin+1));
		hap1[i]=(int *)malloc(sizeof(int)*(numOfSNPInOrigin+1));
	}

	for(i=0;i<numOfRepeat;i++){
		srand(i*1000);
		generateNewDataset(filename);
		firstStep("_randomData.matrix", numOfSNPInOrigin, hap0[i], hap1[i]);	
		score[i]=refineHap(filename, numOfSNPInOrigin, numOfReadsInOrigin, fragList, hap0[i], hap1[i]);
		
		if(score[i]<minScore){
			minScore=score[i];
			indexOfMinScore=i;
		}
	}

	//find top numSelected solutions with minimal score, 
	//if selected, status=1
	selected[0]=indexOfMinScore;
	status[indexOfMinScore]=1;
	for(i=1;i<numSelected;i++){
		tmp=99999999;
		for(j=0;j<numOfRepeat;j++){
			if(status[j]!=1 && score[j]<tmp){
				k=j;
				tmp=score[j];
			}
		}
		selected[i]=k;
		status[k]=1;
	}
	

	//find whether hap0[i] or hap1[i] corresponds to hap0[indexOfMinScore]
	correspondence[0]=0;
	for(i=1;i<numSelected;i++){
		dis0=compDistanceFor2Hap(hap0[indexOfMinScore],hap0[selected[i]], numOfSNPInOrigin);
		dis1=compDistanceFor2Hap(hap0[indexOfMinScore],hap1[selected[i]], numOfSNPInOrigin);
		if(dis0<=dis1)
			correspondence[i]=0;
		else
			correspondence[i]=1;
	}


	for(i=1;i<=numOfSNPInOrigin;i++){
		count00=0;count01=0;count10=0;count11=0;	
		for(j=0;j<numSelected;j++){
			k=selected[j];
			if(correspondence[j]==0){
				if(hap0[k][i]==0)
					count00++;
				else
					count01++;
				if(hap1[k][i]==0)
					count10++;
				else
					count11++;
			}
			else{
				if(hap1[k][i]==0)
					count00++;
				else
					count01++;
				if(hap0[k][i]==0)
					count10++;
				else
					count11++;
			}
		}
		tmp = count01 + count10;
		tmp2 = count00 + count11;
		if (tmp <= tmp2){
			finalHap0[i]=0;
			finalHap1[i]=1;
		}
		else{
			finalHap0[i]=1;
			finalHap1[i]=0;
		}

		/*if(count00>=count01)
			finalHap0[i]=0;
		else
			finalHap0[i]=1;
		if(count10>=count11)
			finalHap1[i]=0;
		else
			finalHap1[i]=1;*/
	}

	
	tmp=refineHap(filename, numOfSNPInOrigin, numOfReadsInOrigin, fragList, finalHap0, finalHap1);
	tmp = getFinalCost(numOfReadsInOrigin, fragList, finalHap0, finalHap1);
	printf("%d\n", tmp);

	out=fopen(filename2, "w+");
	for (i=1; i<=numOfSNPInOrigin; i++)
		fprintf(out, "%d", finalHap0[i]);
	fprintf(out, "\n");
	for (i=1; i<=numOfSNPInOrigin; i++)
		fprintf(out, "%d", finalHap1[i]);
	fclose(out);
}
