# HapAssembly

Implementation of the algorithm in the following paper: 


Fei Deng, Wenjuan Cui and Lusheng Wang. "A highly accurate heuristic algorithm for the haplotype assembly problem." BMC genomics 14.2 (2013): 1.


# How to install

You can compile the program by typing
```
make
```

# How to use
Five parameters should be provided in order to run HapAssembly

* argv[1]: input haplotype file
* argv[2]: output file name
* argv[3]: number of iterations
* argv[4]: number of top candidates to choose
* argv[5]: bound of coverage 

