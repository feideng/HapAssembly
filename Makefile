CC = gcc
CFLAGS  = -g -Wall

default: HapAssembly

HapAssembly:  main.o dataPreprocessing.o filterData.o func.o hapAssembly.o 
	$(CC) $(CFLAGS) -o HapAssembly main.o dataPreprocessing.o filterData.o func.o hapAssembly.o


main.o:  main.c global.h externfunc.h externVar.h
	$(CC) $(CFLAGS) -c main.c

dataPreprocessing.o:  dataPreprocessing.c structDef.h 
	$(CC) $(CFLAGS) -c dataPreprocessing.c

filterData.o:  filterData.c structDef.h externfunc.h externVar.h 
	$(CC) $(CFLAGS) -c filterData.c

func.o:  func.c externVar.h
	$(CC) $(CFLAGS) -c func.c

hapAssembly.o:  hapAssembly.c hapAssembly.h externfunc.h externVar.h 
	$(CC) $(CFLAGS) -c hapAssembly.c
		
clean: 
	$(RM) HapAssembly *.o *~